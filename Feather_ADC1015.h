

//rounding method for high low and middle values.
//NOTE if you change x-0 term, you'll need to consider the case where result can go negative
//roll over to very large number.

//sensitivity.  Greater number, more noise spikes.
#define EWMA_ROUND 3

#define ABS_DELTA(x,y) ( ((x > y)?(x - y):(y - x)) );

// Used to identify a legitimate noise. trigger action above high
#define NOISE_THRESHOLD_HIGH 500
// ignore stuff below low.
#define NOISE_THRESHOLD_LOW 300

//States for pump noise. 
#define C_NOTHING 0
#define C_LOW 1
#define C_HIGH 2



//Delay between samples.
#define CYCLE_DELAY_MS 10

//uint8_t  Max delay is 255.  if want higher will need to change approach.
//use builtin timer?
#define WARMUP_DELAY_COUNT 255



//LED indicators
#define S_LED 12   /* Indications */
#define Y_LED 13   /* Voltage is below threshold */
#define G_LED 14   /* Voltage is above threshold */
#define A_LED 15   /* Indications */

