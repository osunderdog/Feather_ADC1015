#include "ewma.hpp"

EWMA::EWMA(uint8_t resolution)
{
  _resolution = resolution;
  _ewmaValue = 0;
}

const uint8_t EWMA::resolution()
{
  return _resolution;
}

void EWMA::update(const uint16_t value)
{
  _ewmaValue = EWMA_P(_ewmaValue, value, _resolution);
}

const uint16_t EWMA::result()
{
  return _ewmaValue;
}

