#include <Wire.h>
#include <Adafruit_ADS1015.h>

#include "Feather_ADC1015.h"
#include "ewma.hpp"

Adafruit_ADS1015 ads;

EWMA middleValue(5);
EWMA highValue(EWMA_ROUND);
EWMA lowValue(EWMA_ROUND);

uint16_t noiseVolume = 0;

uint8_t noiseState = C_NOTHING;

uint8_t warmup_delay_index = WARMUP_DELAY_COUNT;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  //Serial.begin(250000);
  delay(100);
  Serial.println("Serial connected.");

  pinMode(A_LED, OUTPUT);
  pinMode(G_LED, OUTPUT);
  pinMode(Y_LED, OUTPUT);
  pinMode(S_LED, OUTPUT);

  ads.begin();
}

void loop() {

// EWMA calculations.
// interesting reading:
// http://electronics.stackexchange.com/questions/30370/fast-and-memory-efficient-moving-average-calculation
//


  int16_t adcValue = ads.readADC_SingleEnded(0);

  //keep a middle value
  middleValue.update(adcValue);
  
  //if adcValue is greater than middle value then add it to the EWMA for the high value
  if(adcValue >= middleValue.result())
  {
    highValue.update(adcValue);
  }
 
  if(adcValue <= middleValue.result())
  {
    lowValue.update(adcValue);
  }
  
  noiseVolume = ABS_DELTA(highValue.result(), lowValue.result());

  //Check to see if noiseVolume is above
  //--------------------------------------------------
  // HIGH REGION
  //--------------------------------------------------
  if(noiseVolume > NOISE_THRESHOLD_HIGH) {
    //previous state C_NOTHING..
    if(noiseState == C_HIGH) {
      //was high, is high.
      }
    else if(noiseState == C_NOTHING) {
      //was nothing is high
      //shouldn't get here.
      }
    else if(noiseState == C_LOW) {
      //was low is high
      digitalWrite(G_LED,HIGH);

      if(warmup_delay_index == 0) {
      //do something profound with DASH
      }
      }

    //Setting noiseState to HIGH...
    noiseState = C_HIGH;
    }
    //--------------------------------------------------
    // LOW REGION
    //--------------------------------------------------    
    else if (noiseVolume > NOISE_THRESHOLD_LOW) {
      if (noiseState == C_HIGH) {
        //was high is low.
        //turn off led indicating that pump is on.
        digitalWrite(G_LED,LOW);

        if(warmup_delay_index == 0) {
          //do something profound
        }
      }
      else if(noiseState == C_NOTHING){
        //was nothing is low.
        digitalWrite(Y_LED,HIGH);
      } else {
        //was low is low.
      }
      noiseState = C_LOW;
    } else {
    //--------------------------------------------------
    // BELOW LOW REGION
    //--------------------------------------------------    
      if(noiseState == C_HIGH) {
        //was high is nothing
      }
      else if (noiseState == C_NOTHING) {
        //was nothing is nothing.
      }
      else if (noiseState == C_LOW) {
        //was low is nothing
      }
    digitalWrite(G_LED,LOW);
    digitalWrite(Y_LED,LOW);
    }

    //initial action during warmup.
    if(warmup_delay_index > 0) { 
      //during warmup, go through the motions, but don't trigger servo. 
      digitalWrite(A_LED,HIGH); 
      delay(CYCLE_DELAY_MS); 
      digitalWrite(A_LED,LOW); 
      warmup_delay_index--; 
      } else { 
        delay(CYCLE_DELAY_MS);
      }

  Serial.print(adcValue);
  Serial.print(",");
  Serial.print(noiseVolume);
  Serial.print(",");
  Serial.print(highValue);
  Serial.print(",");
  Serial.print(lowValue);
  Serial.print(",");
  Serial.print(middleValue);
  Serial.println("");
    
}
