#ifndef EWMA_h
#define EWMA_h

#include "Arduino.h"

//fraction of value that is passed forward.
#define FF(x,b) ((x) >> b)

// if newval is less than pval then subtract from rather than add to.
#define EWMA_P(pval,newval,b) ((newval>pval)?(pval + FF((newval - pval),b)):(pval - FF((pval - newval),b)))  


class EWMA
{
public:
  EWMA(uint8_t);

  void update(const uint16_t);

  const uint16_t result();
  const uint8_t resolution();

public:
  //cast to uint16_t when needed by print statements usually.
  operator uint16_t() const {return _ewmaValue;}
  
private:
  uint8_t _resolution;
  uint16_t _ewmaValue;
};


#endif //EWMA_h

